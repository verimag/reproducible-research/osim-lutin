
all: run_expe run_bench

run_expe:
	cd expe ; make 

run_bench:
	cd bench ; make

-include Makefile.local
