#!/bin/bash
set -x

otawa="true"
ogensim="true"
xpdf="true"
OGENSIM=${OGENSIM:-"osim.arm"}
OTAWA=${OTAWA:-"owcet.arm"}
ORANGE=${ORANGE:-"orange"}
LUSTREV6=${LUSTREV6:-"lus2lic"}
fixffx=${fixffx:-"fixffx"}
mkff=${mkff:-"mkff"}
lutin=${lutin:-"lutin"}
rdbg=${rdbg:-"rdbg-batch"}
getstat=${getstat:-"getstat.r"}
gcc=${gcc:-"arm-elf-gcc"}

if [ $# -gt 0 ]
    then
    case "$1" in
        "otawa")
            otawa="true"
            ogensim="false"
            xpdf="false"
            ;;
        "ogensim")
            otawa="false"
            ogensim="true"
            xpdf="false"
            ;;
        "both")
            otawa="true"
            ogensim="true"
            xpdf="false"
            ;;
    esac
fi


# il faut expanser les macros pour orange
cp convertible_main.c convertible_main.orig.c
cpp convertible_main.c > convertible_main_cpp.c



cfile=convertible_main.c
execfile=main.exec
main_step=convertible_main_step
n=main
n_n=convertible_main
freeport=`python -c 'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()'`
if [ "$otawa" = "true" ]
then
# ZZZ otawa won't work with programs that use division because of orange (!?)
# Let's compile the c files for otawa
$gcc --specs=linux.specs -g -o main.exec \
	lustre_consts.c convertible_main.c  convertible_main_main.c > \
	$n_n.owcet.log 2>&1 &&

$ORANGE $cfile ${n_n}_step -o $n_n.ffx > $n_n.orange.log  2>&1 &&

IDIR=`readlink -f fixffx`
IDIR=`dirname "$IDIR"`
ARM_LOOPLOC="$IDIR/arm.looploc"

$mkff -x $execfile > $n_n.ff
$fixffx $ARM_LOOPLOC -i $n_n.ff >  $n_n.fixed.ffx
# Let's  run otawa (owcet.arm)
$OTAWA $execfile $main_step  -f $n_n.fixed.ffx -f $n_n.ffx --add-prop otawa::ilp::OUTPUT_PATH=$main_step.lp \
	>$n_n.owcet.arm.log 2>&1 && 
grep WCET $n_n.owcet.arm.log | cut -d "=" -f 2 > $n.wcet &&

WCET=`cat $n.wcet` 

# Let's compile the c files for ogensim 

fi
if [ "$ogensim" = "true" ]
    then
    ENV=$2
    SEED=$3
    
# Now let's run ogensim 
($gcc --specs=linux.specs -g -o main4ogensim.exec \
	lustre_consts.c convertible_main.c  convertible_main_loop.c >>$n_n.owcet.log  2>&1 &&
$LUSTREV6 convertible.lus -n $n -interface > $n.io &&
$LUSTREV6 convertible.lus -n $n --gen-autotest &&


# Now let's run ogensim 
($OGENSIM main4ogensim.exec -ul 1 \
	-e $main_step -cl $n.cycles-$ENV -lp $freeport \
	-iol $n.io > $n_n.ogensim.log  2>&1&) && 

sleep 1 &&
($rdbg -lurette -l 10000 -o main.rif \
	 --sut-socket "127.0.0.1:$freeport"  \
	 --env-stdio "$lutin -boot -rif env.lut -n $ENV -seed $SEED" || true)) &&

$getstat $n.cycles-$ENV  $WCET > $n-$ENV.stat
fi

xpdf $n.cycles-$ENV.pdf &


